// toggle mobile hidden menu lists
const collapsesArr = document.querySelectorAll('.js-collapse-trigger');
collapsesArr.forEach(function(item) {
    item.onclick = function() {
        item.setAttribute('disabled', true);
        item.classList.toggle('is-active');
        const parent = item.closest('.js-collapse');
        if (parent.classList.contains('is-collapsed')) {
            DOMAnimations.slideUp(this.closest('.js-collapse').children[1], 500);
            setTimeout(() => { parent.classList.remove('is-collapsed'); }, 500);
        } else {
            DOMAnimations.slideDown(this.closest('.js-collapse').children[1], 500);
            setTimeout(() => { parent.classList.add('is-collapsed'); }, 500);
        }
        setTimeout(() => {
            item.removeAttribute('disabled');
        },500);
    }
});