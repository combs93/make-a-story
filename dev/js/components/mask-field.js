const maskFieldsArr = document.querySelectorAll('.form-control-time.maskField');

maskFieldsArr.forEach(function(item) {
    item.oninput = function() {
        if (this.value.length > 0) {
            this.closest('.form-control-group--time').classList.add('is-active')
        } else {
            this.closest('.form-control-group--time').classList.remove('is-active')
        }
    }
});