// open mobile menu
const burgerTrigger = document.querySelector('.js-nav-trigger');
const headerNavigation = document.getElementById('header_navigation_menu');

if (burgerTrigger) {
    burgerTrigger.onclick = function() {
        this.classList.toggle('is-active')
        headerNavigation.classList.toggle('nav-open')
    }
} 

// stopPropagation when ckicking on sublists
const sublistItemsArr = document.querySelectorAll('.header__sublist__item, .header__submenu__item');
sublistItemsArr.forEach(function(item) {
    item.onclick = function(e) {
        if (window.innerWidth < 1280) {
            e.stopPropagation();
        }
    }
});

// toggle mobile hidden menu lists
const subListArr = document.querySelectorAll('.header__nav__item--sub');
subListArr.forEach(function(item) {
    item.onclick = function(e) {
        if (window.innerWidth < 1280) {
            e.stopPropagation();
            item.classList.toggle('is-active');
            DOMAnimations.slideToggle(this.children[1], 300);
        }
    }
});

const subMenuArr = document.querySelectorAll('.header__sublist__item--sub');
subMenuArr.forEach(function(item) {
    item.onclick = function(e) {
        if (window.innerWidth < 1280) {
            e.stopPropagation();
            item.classList.toggle('is-active');
            DOMAnimations.slideToggle(this.children[1], 300);
        }
    }
});