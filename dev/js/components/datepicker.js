document.querySelectorAll('.form-control-datepicker').forEach(function(item) {
    const datePicker = new Lightpick({
        field: item.children[0].querySelector('.form-control'),
        secondField: item.children[1].querySelector('.form-control'),
        singleDate: false,
        lang: 'en',
        format: 'DD MMM YYYY'
    });
});

document.querySelectorAll('.js-datepicker-single').forEach(function(item) {
    const datePicker = new Lightpick({
        field: item,
        singleDate: true,
        lang: 'en',
        format: 'DD MMM YYYY'
    });
});