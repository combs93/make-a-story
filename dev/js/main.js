/* plugins */
//=include plugins/slideToggle.js
//=include ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//=include ../../node_modules/simplebar/dist/simplebar.min.js
//=include ../../node_modules/moment/moment.js
//=include ../../node_modules/lightpick/lightpick.js
//=include ../../node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js
//=include ../../node_modules/supermask-js/index.js


/* components */
//=include components/header.js
//=include components/select.js
//=include components/datepicker.js
//=include components/collapse-blocks.js
//=include components/ckeditor.js
//=include components/mask-field.js

// the main code
svg4everybody();

$('body').on('click', function(e){
	if ($(e.target).closest('.select-wrapper').length === 0) {
		$('.select-wrapper.is-active').removeClass('is-active');
	}
});

$('.select-trigger').on('click', function() {
    $('.select-wrapper.is-active').not($(this).parent()).removeClass('is-active')
});